import badger2040

# @clavimoniere

# Global Constants
display = badger2040.Badger2040()
display.set_update_speed(badger2040.UPDATE_NORMAL)

# default dimensions are 74x71
def tanuki(x=0, y=0, scale=1):
    def xy(a,b):
        return (round(x+scale*a), round(y+scale*b))

    # if using the tanuki function in other draw functions, delete these two lines
    display.set_pen(15)
    display.clear()
    
    display.set_thickness(1)
    
    # top and ears
    display.set_pen(0)
    display.polygon([
        xy(14, 0),
        xy(23, 23),
        xy(51, 23),
        xy(60, 0),
        xy(72, 28),
        xy(37, 50),
        xy(2, 28),
    ])
    
    # left cheek
    display.set_pen(5)
    display.polygon([
        xy(2, 28),
        xy(37, 50),
        xy(23, 61),
        xy(7, 49),
        xy(2, 43),
        xy(0, 37),
        xy(0, 33),
    ])
    
    # right cheek
    display.set_pen(5)
    display.polygon([
        xy(72, 28),
        xy(37, 50),
        xy(51, 61),
        xy(67, 49),
        xy(72, 43),
        xy(74, 37),
        xy(74, 33),
    ])
    
    # boop the snoot
    display.set_pen(10)
    display.polygon([
        xy(37, 50),
        xy(51, 61),
        xy(37, 71),
        xy(23, 61),
    ])
    
    # if using the tanuki function in other draw functions, delete this line
    display.update()
	

# example use: a tanuki centered on the screen

WIDTH = badger2040.WIDTH
HEIGHT = badger2040.HEIGHT
tanuki((WIDTH - 74) // 2, (HEIGHT - 71) // 2)
